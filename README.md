# OpenML dataset: bot-iot-all-features

https://www.openml.org/d/42072

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The BoT-IoT dataset was created by designing a realistic network environment in the Cyber Range Lab of The center of UNSW Canberra Cyber. The environment incorporates a combination of normal and botnet traffic. Free use of the Bot-IoT dataset for academic research purposes is hereby granted in perpetuity. Use for commercial purposes should be agreed by the authors. The authors have asserted their rights under the Copyright. To whom intent the use of the Bot-IoT dataset, please cite the following paper that has the dataset&rsquo;s details.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42072) of an [OpenML dataset](https://www.openml.org/d/42072). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42072/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42072/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42072/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

